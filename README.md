# grpc keep alive

gRPCのKeep Aliveがどう動くのかをテストのための環境作り

## protocでのビルド

`protoc --go_out=plugins=grpc:./ proto/stream.proto`

## コンテナの起動

* 未作成: `docker-compose up -d --build`
* 作成済み: `docker-compose restart`

## サーバの起動

`docker exec -it grpc-server go run server/server.go`

## クライアントの起動

`docker exec -it grpc-client go run client/client.go`

## Kotlinクライアントの起動

`docker exec -it grpc-kotlin ./gradlew run`

## ネットワーク切断・接続

* 切断
    * サーバ側: `docker network disconnect grpc-keep-alive_default grpc-server`
    * Golangクライアント側: `docker network disconnect grpc-keep-alive_default grpc-client`
    * Kotlinクライアント側: `docker network disconnect grpc-keep-alive_default grpc-kotlin`
* 接続
    * サーバ側: `docker network connect grpc-keep-alive_default grpc-server`
    * Golangクライアント側: `docker network connect grpc-keep-alive_default grpc-client`
    * Kotlinクライアント側: `docker network connect grpc-keep-alive_default grpc-kotlin`


## 結果

### Golang Server - Golang Client

|切断側|keep alive|状況|
|---|---|---|
|サーバ|なし|ネットワーク切断と同時にクライアントの受信は止まるが、サーバは送信し続けている。<br>その後、ネットワークに接続したタイミングでクライアントで受信できていなかったメッセージを纏めて受け取り、続きの受信ができた|
|クライアント|なし|ネットワーク切断と同時にクライアントの受信は止まるが、サーバは送信し続けている。<br>その後、ネットワークに接続したタイミングでクライアントで受信できていなかったメッセージを纏めて受け取り、続きの受信ができた|
|サーバ|あり|ネットワーク断から指定時間後に"transport is closing"のエラーが出て終了<br>サーバ側が先に切れても、クライアント側が先に切れても、もう片方は自分の設定時間が経過するまで切れない|
|クライアント|あり|ネットワーク断から指定時間後に"transport is closing"のエラーが出て終了<br>サーバ側が先に切れても、クライアント側が先に切れても、もう片方は自分の設定時間が経過するまで切れない|

### Golang Server - Kotlin Client

|切断側|keep alive|状況|
|---|---|---|
|サーバ|なし|ネットワーク切断と同時にクライアントの受信は止まるが、サーバは送信し続けている。<br>その後、ネットワークに接続したタイミングでクライアントで受信できていなかったメッセージを纏めて受け取り、続きの受信ができた|
|クライアント|なし|ネットワーク切断と同時にクライアントの受信は止まるが、サーバは送信し続けている。<br>その後、ネットワークに接続したタイミングでクライアントではネットワークエラーを検知|
|サーバ|あり|ネットワーク断から指定時間後にサーバ側に"transport is closing"のエラーが出て、クライアント側に"io.grpc.StatusRuntimeException"が出て終了<br>サーバ側が先に切れても、クライアント側が先に切れても、もう片方は自分の設定時間が経過するまで切れない|
|クライアント|あり|ネットワーク断から指定時間後にサーバ側に"transport is closing"のエラーが出て、クライアント側に"io.grpc.StatusRuntimeException"が出て終了<br>サーバ側が先に切れても、クライアント側が先に切れても、もう片方は自分の設定時間が経過するまで切れない|
