package main

import (
	"context"
	"fmt"
	proto "gitlab.com/tsuchinaga/grpc-keep-alive/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"time"
)

func main() {
	fmt.Println("クライアントサイド > こんにちわーるど")

	conn, err := grpc.Dial(
		"grpc-server:5555",
		grpc.WithInsecure(),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                10 * time.Second,
			Timeout:             5 * time.Second,
			PermitWithoutStream: true,
		}))
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	c := proto.NewConnectionServiceClient(conn)

	ctx := context.Background()
	req := new(proto.MessageRequest)
	req.Interval = 3

	stream, err := c.MessageStream(ctx, req)
	if err != nil {
		panic(err)
	}

	for {
		resp, err := stream.Recv()
		if err != nil {
			panic(err)
		}
		fmt.Println(resp)
	}
}
