import io.grpc.Context
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import io.grpc.stub.StreamObserver
import tsuchinaga.grpckeepalive.ConnectionResponse
import tsuchinaga.grpckeepalive.ConnectionServiceGrpc
import tsuchinaga.grpckeepalive.MessageRequest
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

fun main() {
    println("こんにちわーるど")

    val channel = NettyChannelBuilder
        .forAddress("grpc-server", 5555)
        .usePlaintext()
        .keepAliveTime(10, TimeUnit.SECONDS)
        .keepAliveTimeout(5, TimeUnit.SECONDS)
        .keepAliveWithoutCalls(true)
        .build()
    val stub = ConnectionServiceGrpc.newStub(channel)

    val finishStream = CountDownLatch(1)
    val request = MessageRequest.newBuilder().setInterval(3).build()

    val responseObserver = object : StreamObserver<ConnectionResponse> {
        override fun onCompleted() {
            println("onCompleted")
            finishStream.countDown()
        }

        override fun onError(t: Throwable?) {
            println("onError: ${t.toString()}")
            finishStream.countDown()
        }

        override fun onNext(value: ConnectionResponse?) {
            println("onNext: $value")
        }
    }

    val listenRunnable = Runnable { stub.messageStream(request, responseObserver) }
    val cancelContext = Context.current().withCancellation()
    cancelContext.run(listenRunnable)

    finishStream.await()
}
