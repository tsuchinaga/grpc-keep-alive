package main

import (
	"fmt"
	proto "gitlab.com/tsuchinaga/grpc-keep-alive/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"net"
	"time"
)

type server struct {
	proto.ConnectionServiceServer
}

func (s *server) MessageStream(request *proto.MessageRequest, stream proto.ConnectionService_MessageStreamServer) error {
	fmt.Println(request)
	for {
		resp := new(proto.ConnectionResponse)
		resp.Message = "message"
		resp.Datetime = time.Now().Format("2006/01/02 15:04:05")
		resp.Type = proto.ResponseTypeEnum_MESSAGE
		fmt.Println(resp)

		err := stream.Send(resp)
		if err != nil {
			fmt.Println(err)
			break
		}

		time.Sleep(time.Duration(request.Interval) * time.Second)
	}

	return nil
}

func main() {
	fmt.Println("サーバーサイド > こんにちわーるど")

	lis, err := net.Listen("tcp", ":5555")
	if err != nil {
		panic(err)
	}
	s := grpc.NewServer(grpc.KeepaliveParams(keepalive.ServerParameters{
		MaxConnectionIdle:     0,
		MaxConnectionAge:      0,
		MaxConnectionAgeGrace: 0,
		Time:                  5 * time.Second,
		Timeout:               5 * time.Second,
	}))
	proto.RegisterConnectionServiceServer(s, new(server))

	if err := s.Serve(lis); err != nil {
		panic(err)
	}
}
